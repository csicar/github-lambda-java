package evaluation

import com.sun.source.tree.IdentifierTree
import com.sun.source.util.TreeScanner

class ReferenceVisitor : TreeScanner<List<IdentifierTree>, Unit>() {
    override fun visitIdentifier(p0: IdentifierTree, p1: Unit?): List<IdentifierTree> {
        return listOf(p0)
    }

    override fun reduce(p0: List<IdentifierTree>?, p1: List<IdentifierTree>?): List<IdentifierTree> {
        if (p0 == null) return p1 ?: listOf()
        if (p1 == null) return p0
        return p0 + p1
    }
}