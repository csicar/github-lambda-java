package evaluation

import com.sun.source.tree.*
import com.sun.source.util.TreeScanner

class FPVisitor : TreeScanner<Int, Unit?>() {

    override fun visitLambdaExpression(lambdaExp: LambdaExpressionTree, parent: Unit?) : Int? {
        return 1 + super.visitLambdaExpression(lambdaExp, parent)
    }

    override fun visitMemberReference(p0: MemberReferenceTree?, p1: Unit?): Int? {
        val self = if (p0?.mode === MemberReferenceTree.ReferenceMode.INVOKE) 1 else 0
        return self + super.visitMemberReference(p0, p1)
    }

    override fun reduce(p0: Int?, p1: Int?): Int? {
        if (p0 == null) return p1 ?: 0
        if (p1 == null) return p0
        return p0 + p1
    }
}
