package evaluation

import com.sun.source.tree.MethodTree
import com.sun.source.util.TreeScanner
import com.sun.tools.javac.tree.JCTree

data class MethodInfo(val fpCount: Int, val lines: Int, val name: String, val text: String, val method: MethodTree) {
    val fileLine get() = if (method is JCTree) method.pos else -1

}

class MethodVisitor: TreeScanner<List<MethodInfo>, Unit>() {
    override fun visitMethod(p0: MethodTree?, p1: Unit?): List<MethodInfo> {
        if (p0 == null) return emptyList()

        val fpCount = p0.accept(FPVisitor(), null)

        val lines = p0.toString().lines().size
        val name = p0.name.toString()
        return listOf(MethodInfo(fpCount ?: 0, lines, name, p0.toString(), p0))
    }

    override fun reduce(p0: List<MethodInfo>?, p1: List<MethodInfo>?): List<MethodInfo> {
        if (p0 == null) return p1 ?: listOf()
        if (p1 == null) return p0
        return p0 + p1
    }
}