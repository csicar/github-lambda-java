package evaluation

import com.sun.source.tree.Tree
import com.sun.tools.javac.api.JavacTaskImpl
import com.sun.tools.javac.tree.JCTree
import downloadFolder
import github.Repo
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.transactions.transaction
import readRepoInfoFromFile
import java.io.File
import javax.tools.ToolProvider

data class ContextInfo(val file: File, val context: Tree, val outsideReferences: Int, val sideEffecting: SideEffect)

fun main() {
    println("read repo info ...")
    val repos = readRepoInfoFromFile()

    println("create db...")
    createDb()
    transaction {
        println("create schemas ...")
        SchemaUtils.create(LambdaInfo)
        SchemaUtils.create(MethodInfoTable)
    }


    repos.forEachIndexed { index, repo ->
        parseRepoFiles(repo).forEach { fileInfo ->
            val expInfos = fileInfo.lambdaExpressionInfos
            val file = fileInfo.file

            transaction {
                fileInfo.methodInfos.forEach { methodInfo ->
                    MethodInfoTable.insert {
                        it[numberFpFeatures] = methodInfo.fpCount
                        it[linesNumbers] = methodInfo.lines
                        it[name] = methodInfo.name
                        it[text] = methodInfo.text
                        it[fileName] = file.absolutePath
                        it[fileLine] = methodInfo.fileLine
                        it[projectUrl] = repo.downloadUrl
                        it[projectName] = repo.name
                    }
                }
                expInfos.forEach { expInfo ->
                    val lambdaContext = expInfo.lambdaContext
                    LambdaInfo.insert {
                        it[clazz] = lambdaContext::class.java.name.toString()
                        it[name] = lambdaContext.toString()
                        it[type] = lambdaContext.generateType()
                        it[outsideReferences] = expInfo.outsideReferences.size
                        it[LambdaInfo.file] = file.absolutePath
                        it[fileLine] = (if (expInfo.context is JCTree) expInfo.context.pos else -1)
                        it[sourceText] = expInfo.context.toString()
                        it[innerSourceText] = expInfo.exp.toString()
                        it[sideEffecting] = expInfo.sideEffecting.toString()
                        it[bodyKind] = expInfo.bodyKind.toString()
                        it[projectUrl] = repo.downloadUrl
                        it[projectName] = repo.name
                    }
                }
            }
        }
        println()
        println("$index / ${repos.size} (${repo.name})")
        println()
    }


}

fun parseRepoFiles(repo: Repo): Sequence<FileInfo> {
    return File(downloadFolder, repo.folder.toString()).walkTopDown().map {
        if (it.isFile && it.toString().endsWith(".java")) {
            //println("found a java file!$it")
            sequenceOf(parseFile(it))
        } else {
            emptySequence()
        }
    }.flatten()
}

fun parseFile(file: File): FileInfo {
    val compiler = ToolProvider.getSystemJavaCompiler()
    val fileManager = compiler.getStandardFileManager(null, null, null)
    val filename = file.toURI()
    val fileObjects = fileManager.getJavaFileObjects(File(filename))
    val task = compiler.getTask(null, null, null, null, null, fileObjects)
    val javacTask = task as JavacTaskImpl
    val parsed = javacTask.parse() ?: return FileInfo.emptyFileInfo(file)

    val lambdaExpressionInfos = parsed.flatMap {
        it.accept(LambdaExpressionVisitor(), null)
    }

    val fpUseInfo = parsed.flatMap {
        it.accept(MethodVisitor(), null)
    }

    return FileInfo(file, lambdaExpressionInfos, fpUseInfo)
}

fun escapeHTML(s: String): String {
    val out = StringBuilder(Math.max(16, s.length))
    for (i in 0 until s.length) {
        val c = s[i]
        if (c.toInt() > 127 || c == '"' || c == '<' || c == '>' || c == '&') {
            out.append("&#")
            out.append(c.toInt())
            out.append(';')
        } else {
            out.append(c)
        }
    }
    return out.toString()
}

