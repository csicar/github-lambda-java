package evaluation

sealed class Context {
    abstract fun generateType(): String
}
data class InvocationFieldContext(val name: String) : Context() {
    override fun generateType() = name
}

data class InvocationContext(val text: String) : Context() {
    override fun generateType() = text
}

data class ReturnContext(val type: String) : Context() {
    override fun generateType() = type
}

data class NewClassContext(val className: String) : Context() {
    override fun generateType() = className
}

data class OtherContext(val text: String, val type: String) : Context() {
    override fun generateType() = type
}

data class AssignContext(val name: String) : Context() {
    override fun generateType() = name
}