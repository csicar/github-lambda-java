package evaluation

import com.sun.source.tree.*
import com.sun.source.util.TreeScanner
import com.sun.tools.javac.tree.JCTree
import javax.lang.model.element.Name

enum class SideEffect { YES, UNSURE, NO }

class SideEffectFinder(private val lambdaParameters: List<Name>) : TreeScanner<SideEffect, Unit>() {
    private val sideEffectStarts = listOf("add", "set", "remove", "put")
    private val nonSideEffectStart = listOf("get", "has", "contains", "parse", "to")

    override fun visitMethodInvocation(p0: MethodInvocationTree, p1: Unit?): SideEffect {
        val sideEffect : SideEffect = if (p0 is JCTree.JCMethodInvocation) {
            val meth = p0.meth
            if (meth is JCTree.JCFieldAccess) {
                val methodName = meth.name.toString()
                if (lambdaParameters.contains(meth.name)) {
                    SideEffect.NO
                } else if (sideEffectStarts.any { methodName.startsWith(it) }) {
                    SideEffect.YES
                } else if  (nonSideEffectStart.any { methodName.startsWith(it) }) {
                    SideEffect.NO
                } else {
                    SideEffect.UNSURE
                }
            } else {
                SideEffect.UNSURE
            }
        } else {
            SideEffect.UNSURE
        }
        return reduce(super.visitMethodInvocation(p0, p1), sideEffect)
    }

    override fun visitNewClass(p0: NewClassTree?, p1: Unit?): SideEffect {
        return reduce(super.visitNewClass(p0, p1), SideEffect.NO)
    }

    override fun reduce(p0: SideEffect?, p1: SideEffect?): SideEffect {
        if (p0 == null) return p1 ?: SideEffect.UNSURE
        if (p1 == null) return p0
        return minOf(p0, p1)
    }

    override fun visitLiteral(p0: LiteralTree?, p1: Unit?): SideEffect {
        return SideEffect.NO
    }

    override fun visitVariable(p0: VariableTree?, p1: Unit?): SideEffect {
        return SideEffect.NO
    }

    override fun visitReturn(p0: ReturnTree?, p1: Unit?): SideEffect {
        return reduce(super.visitReturn(p0, p1), SideEffect.NO)
    }

    override fun visitIf(p0: IfTree?, p1: Unit?): SideEffect {
        return reduce(super.visitIf(p0, p1), SideEffect.NO)
    }

    override fun visitConditionalExpression(p0: ConditionalExpressionTree?, p1: Unit?): SideEffect {
        return reduce(super.visitConditionalExpression(p0, p1), SideEffect.NO)
    }


}