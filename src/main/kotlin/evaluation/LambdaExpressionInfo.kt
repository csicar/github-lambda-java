package evaluation

import com.sun.source.tree.IdentifierTree
import com.sun.source.tree.LambdaExpressionTree
import com.sun.source.tree.Tree
import com.sun.tools.javac.tree.JCTree
import javax.lang.model.element.Name

data class LambdaExpressionInfo(
    val exp: LambdaExpressionTree,
    val context: Tree?,
    val bodyReferences: List<IdentifierTree>,
    val parameterReferences: List<Name>,
    val sideEffecting: SideEffect?,
    val bodyKind: LambdaExpressionTree.BodyKind
) {
    val outsideReferences: List<Name>
        get() {
            return bodyReferences.map { it.name }.filter { !parameterReferences.contains(it) }
        }

    val lambdaContext: Context
        get() {
            return if (context is JCTree.JCMethodInvocation) {
                val method = context.meth
                if (method is JCTree.JCFieldAccess) {
                    InvocationFieldContext(method.name.toString())
                } else {
                    InvocationContext(method.toString())
                }
            } else if(context is JCTree.JCAssign) {
                AssignContext(context?.lhs.toString())
            }else if(context is JCTree.JCReturn) {
                ReturnContext(context?.kind.toString())
            } else if(context is JCTree.JCNewClass) {
                NewClassContext(context.clazz.toString())
            } else {
                OtherContext(context.toString(), context?.kind.toString())
            }
        }
}