package evaluation

import com.sun.source.tree.*
import com.sun.source.util.TreeScanner

class LambdaExpressionVisitor : TreeScanner<List<LambdaExpressionInfo>, Tree?>() {

    override fun visitLambdaExpression(lambdaExp: LambdaExpressionTree, parent: Tree?): List<LambdaExpressionInfo>? {

        val references = lambdaExp.body.accept(ReferenceVisitor(), null) ?: listOf()
        val parameterReferences = lambdaExp.parameters.map { it.name }
        val sideEffect = lambdaExp.body.accept(SideEffectFinder(parameterReferences), null)
        val bodyKind = lambdaExp.bodyKind
        return listOf(LambdaExpressionInfo(lambdaExp, parent, references, parameterReferences, sideEffect, bodyKind))
    }

    override fun reduce(p0: List<LambdaExpressionInfo>?, p1: List<LambdaExpressionInfo>?): List<LambdaExpressionInfo>? {
        if (p0 == null) return p1 ?: listOf()
        if (p1 == null) return p0
        return p0 + p1
    }

    override fun visitExpressionStatement(p0: ExpressionStatementTree?, p1: Tree?): List<LambdaExpressionInfo>? {
        return super.visitExpressionStatement(p0, p0)
    }

    override fun visitMethod(p0: MethodTree?, p1: Tree?): List<LambdaExpressionInfo>? {
        return super.visitMethod(p0, p0)
    }

    override fun visitLiteral(p0: LiteralTree?, p1: Tree?): List<LambdaExpressionInfo>? {
        return super.visitLiteral(p0, p0)
    }

    override fun visitBlock(p0: BlockTree?, p1: Tree?): List<LambdaExpressionInfo>? {
        return super.visitBlock(p0, p0)
    }

    override fun visitWildcard(p0: WildcardTree?, p1: Tree?): List<LambdaExpressionInfo>? {
        return super.visitWildcard(p0, p0)
    }

    override fun visitBreak(p0: BreakTree?, p1: Tree?): List<LambdaExpressionInfo>? {
        return super.visitBreak(p0, p0)
    }

    override fun visitCatch(p0: CatchTree?, p1: Tree?): List<LambdaExpressionInfo>? {
        return super.visitCatch(p0, p0)
    }

    override fun visitNewArray(p0: NewArrayTree?, p1: Tree?): List<LambdaExpressionInfo>? {
        return super.visitNewArray(p0, p0)
    }

    override fun visitForLoop(p0: ForLoopTree?, p1: Tree?): List<LambdaExpressionInfo>? {
        return super.visitForLoop(p0, p0)
    }

    override fun visitModifiers(p0: ModifiersTree?, p1: Tree?): List<LambdaExpressionInfo>? {
        return super.visitModifiers(p0, p0)
    }

    override fun visitCase(p0: CaseTree?, p1: Tree?): List<LambdaExpressionInfo>? {
        return super.visitCase(p0, p0)
    }

    override fun visitErroneous(p0: ErroneousTree?, p1: Tree?): List<LambdaExpressionInfo>? {
        return super.visitErroneous(p0, p0)
    }

    override fun visitEmptyStatement(p0: EmptyStatementTree?, p1: Tree?): List<LambdaExpressionInfo>? {
        return super.visitEmptyStatement(p0, p0)
    }

    override fun visitTry(p0: TryTree?, p1: Tree?): List<LambdaExpressionInfo>? {
        return super.visitTry(p0, p0)
    }

    override fun visitVariable(p0: VariableTree?, p1: Tree?): List<LambdaExpressionInfo>? {
        return super.visitVariable(p0, p0)
    }

    override fun scan(p0: Tree?, p1: Tree?): List<LambdaExpressionInfo>? {
        return super.scan(p0, p1)
    }

    override fun scan(p0: MutableIterable<Tree>?, p1: Tree?): List<LambdaExpressionInfo>? {
        return super.scan(p0, p1)
    }

    override fun visitIdentifier(p0: IdentifierTree?, p1: Tree?): List<LambdaExpressionInfo>? {
        return super.visitIdentifier(p0, p0)
    }

    override fun visitCompilationUnit(p0: CompilationUnitTree?, p1: Tree?): List<LambdaExpressionInfo>? {
        return super.visitCompilationUnit(p0, p0)
    }

    override fun visitPrimitiveType(p0: PrimitiveTypeTree?, p1: Tree?): List<LambdaExpressionInfo>? {
        return super.visitPrimitiveType(p0, p0)
    }

    override fun visitLabeledStatement(p0: LabeledStatementTree?, p1: Tree?): List<LambdaExpressionInfo>? {
        return super.visitLabeledStatement(p0, p0)
    }

    override fun visitMemberSelect(p0: MemberSelectTree?, p1: Tree?): List<LambdaExpressionInfo>? {
        return super.visitMemberSelect(p0, p0)
    }

    override fun visitArrayType(p0: ArrayTypeTree?, p1: Tree?): List<LambdaExpressionInfo>? {
        return super.visitArrayType(p0, p0)
    }

    override fun visitMemberReference(p0: MemberReferenceTree?, p1: Tree?): List<LambdaExpressionInfo>? {
        return super.visitMemberReference(p0, p0)
    }

    override fun visitParameterizedType(p0: ParameterizedTypeTree?, p1: Tree?): List<LambdaExpressionInfo>? {
        return super.visitParameterizedType(p0, p0)
    }

    override fun visitNewClass(p0: NewClassTree?, p1: Tree?): List<LambdaExpressionInfo>? {
        return super.visitNewClass(p0, p0)
    }

    override fun visitParenthesized(p0: ParenthesizedTree?, p1: Tree?): List<LambdaExpressionInfo>? {
        return super.visitParenthesized(p0, p0)
    }

    override fun visitThrow(p0: ThrowTree?, p1: Tree?): List<LambdaExpressionInfo>? {
        return super.visitThrow(p0, p0)
    }

    override fun visitWhileLoop(p0: WhileLoopTree?, p1: Tree?): List<LambdaExpressionInfo>? {
        return super.visitWhileLoop(p0, p0)
    }

    override fun visitAssignment(p0: AssignmentTree?, p1: Tree?): List<LambdaExpressionInfo>? {
        return super.visitAssignment(p0, p0)
    }

    override fun visitMethodInvocation(p0: MethodInvocationTree?, p1: Tree?): List<LambdaExpressionInfo>? {
        return super.visitMethodInvocation(p0, p0)
    }

    override fun visitTypeParameter(p0: TypeParameterTree?, p1: Tree?): List<LambdaExpressionInfo>? {
        return super.visitTypeParameter(p0, p0)
    }

    override fun visitOther(p0: Tree?, p1: Tree?): List<LambdaExpressionInfo>? {
        return super.visitOther(p0, p0)
    }

    override fun visitSynchronized(p0: SynchronizedTree?, p1: Tree?): List<LambdaExpressionInfo>? {
        return super.visitSynchronized(p0, p0)
    }

    override fun visitEnhancedForLoop(p0: EnhancedForLoopTree?, p1: Tree?): List<LambdaExpressionInfo>? {
        return super.visitEnhancedForLoop(p0, p0)
    }

    override fun visitIf(p0: IfTree?, p1: Tree?): List<LambdaExpressionInfo>? {
        return super.visitIf(p0, p0)
    }

    override fun visitCompoundAssignment(p0: CompoundAssignmentTree?, p1: Tree?): List<LambdaExpressionInfo>? {
        return super.visitCompoundAssignment(p0, p0)
    }

    override fun visitInstanceOf(p0: InstanceOfTree?, p1: Tree?): List<LambdaExpressionInfo>? {
        return super.visitInstanceOf(p0, p0)
    }

    override fun visitAssert(p0: AssertTree?, p1: Tree?): List<LambdaExpressionInfo>? {
        return super.visitAssert(p0, p0)
    }

    override fun visitTypeCast(p0: TypeCastTree?, p1: Tree?): List<LambdaExpressionInfo>? {
        return super.visitTypeCast(p0, p0)
    }

    override fun visitIntersectionType(p0: IntersectionTypeTree?, p1: Tree?): List<LambdaExpressionInfo>? {
        return super.visitIntersectionType(p0, p0)
    }

    override fun visitAnnotation(p0: AnnotationTree?, p1: Tree?): List<LambdaExpressionInfo>? {
        return super.visitAnnotation(p0, p0)
    }

    override fun visitClass(p0: ClassTree?, p1: Tree?): List<LambdaExpressionInfo>? {
        return super.visitClass(p0, p0)
    }

    override fun visitUnionType(p0: UnionTypeTree?, p1: Tree?): List<LambdaExpressionInfo>? {
        return super.visitUnionType(p0, p0)
    }

    override fun visitDoWhileLoop(p0: DoWhileLoopTree?, p1: Tree?): List<LambdaExpressionInfo>? {
        return super.visitDoWhileLoop(p0, p0)
    }

    override fun visitBinary(p0: BinaryTree?, p1: Tree?): List<LambdaExpressionInfo>? {
        return super.visitBinary(p0, p0)
    }

    override fun visitAnnotatedType(p0: AnnotatedTypeTree?, p1: Tree?): List<LambdaExpressionInfo>? {
        return super.visitAnnotatedType(p0, p0)
    }

    override fun visitUnary(p0: UnaryTree?, p1: Tree?): List<LambdaExpressionInfo>? {
        return super.visitUnary(p0, p0)
    }

    override fun visitConditionalExpression(p0: ConditionalExpressionTree?, p1: Tree?): List<LambdaExpressionInfo>? {
        return super.visitConditionalExpression(p0, p0)
    }

    override fun visitContinue(p0: ContinueTree?, p1: Tree?): List<LambdaExpressionInfo>? {
        return super.visitContinue(p0, p0)
    }

    override fun visitSwitch(p0: SwitchTree?, p1: Tree?): List<LambdaExpressionInfo>? {
        return super.visitSwitch(p0, p0)
    }

    override fun visitImport(p0: ImportTree?, p1: Tree?): List<LambdaExpressionInfo>? {
        return super.visitImport(p0, p0)
    }

    override fun visitArrayAccess(p0: ArrayAccessTree?, p1: Tree?): List<LambdaExpressionInfo>? {
        return super.visitArrayAccess(p0, p0)
    }

    override fun visitReturn(p0: ReturnTree?, p1: Tree?): List<LambdaExpressionInfo>? {
        return super.visitReturn(p0, p0)
    }
}