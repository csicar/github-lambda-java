package evaluation

import java.io.File

data class FileInfo(val file: File, val lambdaExpressionInfos: List<LambdaExpressionInfo>, val methodInfos: List<MethodInfo>) {
    companion object {
        fun emptyFileInfo(file: File) = FileInfo(file, emptyList(), emptyList())
    }
}