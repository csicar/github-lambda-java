package evaluation

import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.Table
import org.jetbrains.exposed.sql.transactions.TransactionManager
import org.sqlite.SQLiteDataSource
import java.io.File
import java.sql.Connection
import javax.xml.crypto.Data

object LambdaInfo : Table() {
    val id = integer("id").autoIncrement().primaryKey()
    val clazz = varchar("clazz", 100)
    val name = text("name")
    val type = varchar("type", 255)
    val outsideReferences = integer("outside_references")
    val file = varchar("file", 500)
    val sideEffecting = varchar("side_effecting", 100)
    val bodyKind = varchar("body_kind", 100)
    val fileLine = integer("file_line")
    val sourceText = text("source_text")
    val innerSourceText = text("inner_source_text")
    val projectUrl = varchar("project_url", 300)
    val projectName = varchar("project_name", 200)
}

object MethodInfoTable : Table() {
    val id = integer("id").autoIncrement().primaryKey()
    val numberFpFeatures = integer("number_fp_features")
    val linesNumbers = integer("number_lines")
    val name = varchar("name", 200)
    val fileName = varchar("file_name", 500)
    val fileLine = integer("file_line")
    val projectUrl = varchar("project_url", 300)
    val projectName = varchar("project_name", 200)
    val text = text("source_text")
}

fun createDb() {
    val file = File("/tmp/database.sqlite")
    file.createNewFile()

    val filename = file.absolutePath
    val ds = SQLiteDataSource()
    ds.url = "jdbc:sqlite:$filename"
    Database.connect(ds)
    TransactionManager.manager.defaultIsolationLevel = Connection.TRANSACTION_SERIALIZABLE
}