package github

import downloadFolder
import gson
import khttp.structures.authorization.BasicAuthorization
import org.json.JSONObject
import readRepoInfoFromFile
import token
import user
import java.io.File
import java.io.FileOutputStream

fun main(numberOfSearchPages: Int = 30) {
    fetchZipFiles(numberOfSearchPages)
    val repos = readRepoInfoFromFile()
    repos.forEach {
        it.unzipFiles()
    }
}

fun fetchZipFiles(numberOfSearchPages: Int) {
    val repos = IntRange(1, numberOfSearchPages).flatMap { fetchRepositories(it) }

    File("./repos.json").writeText(gson.toJson(repos))
    println(gson.toJson(repos))

    repos.forEach {
        println("started: " + it.name)
        downloadFromUrl(it.downloadUrl, it.folder)
        println("finished: " + it.name)
        Thread.sleep(1000)
    }
}

fun fetchRepositories(page: Int = 1, perPage: Int = 100): List<Repo> {
    val res = khttp
        .get(
            "https://api.github.com/search/repositories",
            params = mapOf(
                "q" to "language:java",
                "sort" to "stars",
                "order" to "desc",
                "page" to "$page",
                "per_page" to "$perPage"
            ),
            auth = getAuth()
        )
    println(res.url)
    if (!res.jsonObject.has("items")) {
        println("Error when parsing result:")
        println(res.jsonObject.toString())
    }
    val items = res.jsonObject.getJSONArray("items")
    return items.toList().map {
        if (it is JSONObject) {
            Repo(it.getString("full_name"), it)
        } else {
            println(it)
            TODO()
        }
    }
}

fun downloadFromUrl(url: String, path: File) {
    val zipFile = File(downloadFolder, path.toString() + ".zip")

    if (zipFile.exists()) return

    val res = khttp.get(url, stream = true, auth = getAuth())
    downloadFolder.mkdirs()

    println(zipFile.absolutePath)
    zipFile.createNewFile()
    res.raw.copyTo(FileOutputStream(zipFile))
}

private fun getAuth() = (if (token != null && user != null) BasicAuthorization(user, token) else null)
