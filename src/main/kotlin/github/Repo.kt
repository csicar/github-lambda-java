package github

import downloadFolder
import org.json.JSONObject
import java.io.File

data class Repo(val name: String, val info: JSONObject, var folder: File = File(
    name.replace(
        "/",
        "_"
    )
)
) {
    fun unzipFiles() {
        val repoZip = File(downloadFolder, folder.toString() + ".zip").absolutePath
        val repoFolder = File(downloadFolder, folder.toString())
        if (repoFolder.exists()) return
        ProcessBuilder()
            .command("unzip", repoZip, "-d", repoFolder.absolutePath)
            .redirectError(ProcessBuilder.Redirect.INHERIT)
            .redirectOutput(ProcessBuilder.Redirect.INHERIT)
            .start()
            .waitFor()
    }

    val downloadUrl: String
        get() {
            return info
                .getString("archive_url")
                .replace("{archive_format}", "zipball")
                .replace("{/ref}", "")
        }

}
