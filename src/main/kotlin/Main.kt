import com.google.gson.Gson
import java.io.File
import github.Repo


val gson = Gson()


fun main() {
    github.main(10)
    evaluation.main()
    println("done!")
}


val downloadFolder = File("../downloaded-repos")

fun readRepoInfoFromFile(): List<Repo> {
    val repos = gson.fromJson(File("./repos.json").readText(Charsets.UTF_8), Array<Repo>::class.java).asList()
    return repos
}